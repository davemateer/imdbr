﻿using System;
using System.Collections.Generic;
using Dapper;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using CsvHelper;
using Npgsql;
using PostgreSQLCopyHelper;
using Serilog;

var sw = Stopwatch.StartNew();
SetupLogger();
using var db = GetOpenConnection();

// assume db created and schema is there
// Clear down db first (using Dapper)
db.Execute($"DELETE FROM rating");
db.Execute($"DELETE FROM title");

// 1.Extract Ratings
using var reader = new StreamReader(@"c:\dev\r\imdbr\data\title-ratings.tsv");
using var csv = new CsvReader(reader, CultureInfo.InvariantCulture);
csv.Configuration.Delimiter = "\t";
var ratingImports = csv.GetRecords<RatingImport>().ToList();

// 1,089,499
Log.Information($"Total ratings loaded is {ratingImports.Count:N0}");

var ratings = new List<Rating>();
foreach (var ratingImport in ratingImports)
{
    // Check for leading or trailing whitespace
    if (ContainsLeadingOrTrailingWhitespace(ratingImport.tconst)) Log.Warning($"WS in rating.tconst {ratingImport.tconst}");
    if (ContainsLeadingOrTrailingWhitespace(ratingImport.averageRating)) Log.Warning($"WS in rating.averageRating {ratingImport.averageRating}");
    if (ContainsLeadingOrTrailingWhitespace(ratingImport.numVotes)) Log.Warning($"WS in rating.numVotes {ratingImport.numVotes}");

    if (string.IsNullOrWhiteSpace(ratingImport.tconst)) Log.Warning($"null empty or whitespace");
    if (string.IsNullOrWhiteSpace(ratingImport.averageRating)) Log.Warning($"null empty or whitespace");
    if (string.IsNullOrWhiteSpace(ratingImport.numVotes)) Log.Warning($"null empty or whitespace");
    // Check for interesting/bad/unusual characters that could affect results
    // eg LF?.. all queries are paramerterised so no problems with ' chars
    // okay due to db handing unicode UTF-8 and using nvarchar to hold strings

    // Custom validation
    //if (!IsSexACapitalMOrF(actor.sex))
    //    Log.Warning("non M or F in Actor sex column");

    var d = double.TryParse(ratingImport.averageRating, out var dout);
    if (!d) throw new ArgumentException("problem with double");
    var i = int.TryParse(ratingImport.numVotes, out var iout);
    if (!i) throw new ArgumentException("problem with int");

    // convert to C# types
    var rating = new Rating
    {
        TConst = ratingImport.tconst,
        AverageRating = dout,
        NumVotes = iout
    };
    ratings.Add(rating);

    //var sql = @"
    //    INSERT INTO rating(tconst, average_rating, num_votes)
    //    VALUES (@TConst, @AverageRating, @NumVotes)";
    //db.Execute(sql, rating);
}
Log.Information("bulk rating copy");
var insertb = new PostgreSQLCopyHelper<Rating>("public", "rating")
    .MapText("tconst", x => x.TConst)
    .MapDouble("average_rating", x => x.AverageRating)
    .MapInteger("num_votes", x => x.NumVotes);
var countOfRatingsWritten = insertb.SaveAll(GetOpenConnection(), ratings);
Log.Information($"{countOfRatingsWritten:N0} ratings inserted");


// 2. Extract titles
Log.Information("Loading Titles");
using var readerb = new StreamReader(@"c:\dev\r\imdbr\data\title-basics.tsv");
using var csvb = new CsvReader(readerb, CultureInfo.InvariantCulture);
csvb.Configuration.Delimiter = "\t";
// double quotes " are used if we need a comma in the csv data
// so we are using TSV https://en.wikipedia.org/wiki/Tab-separated_values so don't need to worry about this
csvb.Configuration.IgnoreQuotes = true;

var titleImports = csvb.GetRecords<TitleImport>().ToList();

// 7,330,433
Log.Information($"Total titles loaded is {titleImports.Count:N0}");

var titles = new List<Title>();
foreach (var titleImport in titleImports)
{
    switch (titleImport.titleType)
    {
        case "movie":
            break; // out of the switch
        case "tvShort":
        case "tvMovie":
        case "short":
        case "tvMiniSeries":
        case "videoGame":
        case "tvEpisode":
        case "video":
        case "tvSpecial":
        case "tvSeries":
            continue; // the foreach
        default:
            throw new ArgumentException($"new titleType {titleImport.titleType}");
    }

    // Check for leading or trailing whitespace
    if (ContainsLeadingOrTrailingWhitespace(titleImport.tconst)) Log.Warning($"WS in title.tconst {titleImport}");
    if (ContainsLeadingOrTrailingWhitespace(titleImport.titleType)) Log.Warning($"WS in title.titleType {titleImport}");
    if (ContainsLeadingOrTrailingWhitespace(titleImport.primaryTitle)) Log.Warning($"WS in title.primaryTitle {titleImport}");
    if (ContainsLeadingOrTrailingWhitespace(titleImport.startYear)) Log.Warning($"WS in title.startYear {titleImport}");
    if (ContainsLeadingOrTrailingWhitespace(titleImport.genres)) Log.Warning($"WS in title.genres {titleImport}");

    // totally blank fields
    if (string.IsNullOrWhiteSpace(titleImport.tconst)) Log.Warning($"null empty or whitespace");
    if (string.IsNullOrWhiteSpace(titleImport.titleType)) Log.Warning($"null empty or whitespace");
    if (string.IsNullOrWhiteSpace(titleImport.primaryTitle)) Log.Warning($"null empty or whitespace");
    if (string.IsNullOrWhiteSpace(titleImport.startYear)) Log.Warning($"null empty or whitespace");
    if (string.IsNullOrWhiteSpace(titleImport.genres)) Log.Warning($"null empty or whitespace");
    // Check for interesting/bad/unusual characters that could affect results
    // eg LF?.. all queries are paramerterised so no problems with ' chars
    // okay due to db handing unicode UTF-8 and using nvarchar to hold strings

    // eg movie, short
    if (titleImport.titleType == @"\N") throw new ApplicationException("problem no title type");

    // default year of 0 if \N
    int iout;
    var i = int.TryParse(titleImport.startYear, out iout);
    int? ioutb = null;
    if (!i)
    {
        if (titleImport.startYear == @"\N") { }
        else
            throw new ArgumentException("title startYear problem");
    }
    else
        ioutb = iout;


    string? genres = null;
    if (titleImport.genres == @"\N") { }
    else
        genres = titleImport.genres;

    // convert to C# types
    var title = new Title
    {
        TConst = titleImport.tconst,
        TitleType = titleImport.titleType,
        PrimaryTitle = titleImport.primaryTitle,
        StartYear = ioutb,
        Genres = genres
    };

    //var sql = @"
    //            INSERT INTO title(tconst, title_type, primary_title, start_year, genres)
    //            VALUES (@TConst,@TitleType, @PrimaryTitle, @StartYear, @Genres)";

    //db.Execute(sql, title);
    titles.Add(title);
}

Log.Information("bulk title copy");
var insert = new PostgreSQLCopyHelper<Title>("public", "title")
    .MapText("tconst", x => x.TConst)
    .MapText("title_type", x => x.TitleType)
    .MapText("primary_title", x => x.PrimaryTitle)
    .MapInteger("start_year", x => x.StartYear)
    .MapText("genres", x => x.Genres);
var countOfTitlesWritten = insert.SaveAll(GetOpenConnection(), titles);
Log.Information($"{countOfTitlesWritten:N0} titles inserted");

// 28m 09s on 4790 with PK's and constraints
// 26:17 without PK's and constraints
Log.Information($"done in mm:ss {sw.Elapsed.Minutes:D2}:{sw.Elapsed.Seconds:D2}");

bool ContainsLeadingOrTrailingWhitespace(string s)
{
    if (string.IsNullOrWhiteSpace(s)) return true;

    var trimmed = s.Trim();
    if (trimmed.Length < s.Length) return true;

    return false;
}

NpgsqlConnection GetOpenConnection()
{
    var connectionString = "Host=localhost;Username=alice;Password=letmein2;Database=imdbr";
    var cnn = new NpgsqlConnection(connectionString);
    cnn.Open();
    return cnn;
}

//IDbConnection GetOpenConnectionForDapper()
//{
//    var connectionString = "Host=localhost;Username=alice;Password=letmein2;Database=imdbr";
//    DbConnection cnn = new NpgsqlConnection(connectionString);
//    return cnn;
//}

void SetupLogger()
{
    // want separate files for each run
    var hms = DateTime.Now.ToString("MMdd-HHmmss");

    Log.Logger = new LoggerConfiguration()
        .WriteTo.Console()
        .WriteTo.File($"../../../logs/{hms}.txt")
        .CreateLogger();
    Log.Information("Starting");
}

class Title
{
    public string TConst { get; set; }
    public string TitleType { get; set; }
    public string PrimaryTitle { get; set; }
    public int? StartYear { get; set; }
    public string? Genres { get; set; }
}

class TitleImport
{
    public string tconst { get; set; }
    public string titleType { get; set; }
    public string primaryTitle { get; set; }
    public string startYear { get; set; }
    public string genres { get; set; }

    public override string ToString() =>
        $"tconst:{tconst}:titleType:{titleType}:primaryTitle:{primaryTitle}startYear:{startYear}:genres{genres}";
}

class Rating
{
    public string TConst { get; set; }
    public double AverageRating { get; set; }
    //public double AverageRating { get; set; }
    public int NumVotes { get; set; }
}

// types must be defined at the bottom of the file
class RatingImport
{
    // favouring the simplest data type string in this load
    // until I understand the data (ie what edge cases are there)
    // yikes - naming convention need to refactor!
    public string tconst { get; set; }
    public string averageRating { get; set; }
    public string numVotes { get; set; }
}



