create database imdbr

-- dev
-- CREATE ROLE alice SUPERUSER LOGIN PASSWORD 'letmein2';

--drop table rating
CREATE TABLE rating (
	tconst text UNIQUE NOT NULL primary key,
  -- precision of 3 and scale of 1 eg 10.0 is max and 5.5, 0.0
	-- average_rating NUMERIC(3,1) NOT NULL,
  average_rating DOUBLE PRECISION NOT NULL,
	num_votes integer
);


--drop table title
CREATE TABLE title (
	tconst text UNIQUE NOT NULL primary key,
  title_type text NOT NULL,
  primary_title text NOT NULL,
  start_year integer,
  genres text
)
-- type a tab (in notepad)
-- 6secs
-- fast but doesn't handle ints nulls etc..
COPY ratingInbound FROM 'C:/dev/r/imdbr/data/title-ratings.tsv' DELIMITER '	'

select count(*) from rating
select * from rating limit 100

select count(*) from title

SELECT tconst INTO TABLE thing
from title 
where start_year < 2021

-- not supported here
SELECT tconst
INTO TABLE rating_2
FROM rating


drop table thing

ALTER TABLE thing ADD PRIMARY KEY (tconst);

select * from thing
order by start_year desc

-- 255,319
-- 558,505
-- 487,254
select count(*) from thing

select * from thing
order by start_year desc

select * from thing
where num_votes is not NULL
order by num_votes desc



select * from title
where start_year > 1900 
order by start_year desc
limit 500

select DISTINCT(title_type)
from title




- -- for testing Register and Login
-- -- from Tardis
-- CREATE TABLE login (
-- 	login_id integer generated always AS IDENTITY primary key,
-- 	email text UNIQUE NOT NULL,
-- 	password_hash text NOT NULL,
-- 	verified boolean NOT NULL
-- );

CREATE INDEX login_email_index ON login (email);

-- CREATE TABLE account (
-- 	account_id integer generated always AS IDENTITY primary key,
-- 	login_id integer REFERENCES login (login_id),
-- 	account_name text NOT NULL
-- );

-- CREATE TABLE trans (
-- 	trans_id integer generated always AS IDENTITY primary key,
-- 	account_id integer REFERENCES account (account_id),
-- 	trans_date timestamp NOT NULL,
-- 	amount money NOT NULL,
-- 	balance money NOT NULL
-- );

-- CREATE TABLE schedule (
-- 	schedule_id integer generated always AS IDENTITY primary key,
-- 	account_id integer REFERENCES account (account_id),
-- 	time_period text NOT NULL CHECK (time_period IN ('day', 'week', 'month')),
-- 	next_run date NOT NULL,
-- 	amount money NOT NULL
-- );


-- for DBTest
CREATE TABLE employee (
    first_name varchar (99),
    last_name varchar (100), 
    address varchar (4000)
);

-- rob's cassini
-- no pk yet (applied after the import_data)
create table master_plan(
  date text,
  team text,
  target text,
  title text,
  description text
);

COPY master_plan
FROM '/home/dave/source/infra/master_plan.csv'
WITH DELIMITER ',' HEADER CSV;;

alter table master_plan
add id serial primary key;

INSERT INTO employee (first_name, last_name, address) VALUES ('John', 'Smith', '123 Duane St');

INSERT INTO employee (first_name, last_name, address) VALUES ('Dave', 'Mateer', '1 Main St');
INSERT INTO employee (first_name, last_name, address) VALUES ('Dave2', 'Mateer2', '2 Main St');
INSERT INTO employee (first_name, last_name, address) VALUES ('Dave3', 'Mateer3', '3 Main St');
INSERT INTO employee (first_name, last_name, address) VALUES ('Dave4', 'Mateer4', '4 Main St');
